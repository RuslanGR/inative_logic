"""Admin"""

from django.contrib import admin

from ..models import InativeUser


class InativeUserAdmin(admin.ModelAdmin):

    list_display = ()

    class Meta:
        model = InativeUser


admin.site.register(InativeUser, InativeUserAdmin)
