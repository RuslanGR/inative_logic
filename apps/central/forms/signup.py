from django.contrib.auth.forms import UserCreationForm

from ..models import InativeUser


class SignupForm(UserCreationForm):
    """User information form"""

    class Meta:
        """Meta"""

        model = InativeUser
        fields = ('email', 'password1', 'password2')

    def save(self, commit=True):
        """Changed save function"""
        user = super(SignupForm, self).save(commit=False)

        if commit:
            user.save()
        return user
