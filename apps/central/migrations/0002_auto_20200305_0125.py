# Generated by Django 3.0.3 on 2020-03-04 22:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('central', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='inativeuser',
            name='address',
        ),
        migrations.RemoveField(
            model_name='inativeuser',
            name='city',
        ),
        migrations.RemoveField(
            model_name='inativeuser',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='inativeuser',
            name='last_name',
        ),
        migrations.RemoveField(
            model_name='inativeuser',
            name='patronymic',
        ),
        migrations.RemoveField(
            model_name='inativeuser',
            name='phone',
        ),
        migrations.RemoveField(
            model_name='inativeuser',
            name='republic',
        ),
        migrations.RemoveField(
            model_name='inativeuser',
            name='zipcode',
        ),
    ]
