from apps.shop import models
from apps.shop.utils import generator


def bind_session_cart(request, user, **kwargs):
    """Bind cart in session to user cart"""
    try:
        if user.cart_set.all()[0]:
            print('has cart', user.cart_set.all()[0])
            return
    except IndexError:
        pass

    try:
        cart_id = request.session['cart_id']
    except KeyError:
        cart_id = generator(size=20)
        request.session['cart_id'] = cart_id

    cart, cart_created = models.Cart.objects.get_or_create(session=cart_id)
    request.session['products_total'] = cart.cartitem_set.count()

    cart.user = user
