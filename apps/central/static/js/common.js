$(function() {

	$("img.lazy").lazyload(); 

	$('nav#menu').mmenu({
		extensions: ["theme-dark"],
		navbar: {
			title: '<img src="img/@2x/logotext.png" class="mm-image" alt="">'
		}
	});

	var ham_api = $('nav#menu').data('mmenu');
	ham_api.bind('openPanel:start', function() {
		console.log('opened');
		$('.hamburger').addClass('is-active');
	});

});
