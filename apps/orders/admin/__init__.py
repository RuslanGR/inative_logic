"""Admin"""

from django.contrib import admin

from .order import Order, OrderAdmin


admin.site.register(Order, OrderAdmin)
