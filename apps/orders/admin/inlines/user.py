from django.contrib import admin

from apps.central.models import InativeUser


class UserInline(admin.TabularInline):
    model = InativeUser
