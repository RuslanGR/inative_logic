from django.contrib import admin

from ..models import Order
# from .inlines import UserInline


class OrderAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('order_id', 'status', 'created', 'total_price')
    # inlines = (UserInline,)

    class Meta:
        model = Order
