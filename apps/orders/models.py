from django.db import models
from django.contrib.auth import get_user_model

from apps.shop.models import Cart


User = get_user_model()


class Order(models.Model):
    """Order model"""

    STATUS_STARTED_CHOICE = 0
    STATUS_ABANDONED_CHOICE = 1
    STATUS_FINISHED_CHOICE = 2

    STATUS_CHOICES = (
        (STATUS_STARTED_CHOICE, 'started'),
        (STATUS_ABANDONED_CHOICE, 'abandoned'),
        (STATUS_FINISHED_CHOICE, 'finished'),
    )

    total_price = models.DecimalField(verbose_name='Итоговая цена', decimal_places=2, max_digits=10,
                                      blank=True, null=True)
    user = models.ForeignKey(verbose_name='Пользователь', to=User, blank=True, null=True, on_delete=models.SET_NULL)
    order_id = models.CharField(verbose_name='ID заказа', max_length=120, default='ABC', unique=True)
    cart = models.ForeignKey(verbose_name='Корзина', to=Cart, on_delete=models.CASCADE)
    status = models.PositiveIntegerField(verbose_name='Статус', choices=STATUS_CHOICES,
                                         default=STATUS_STARTED_CHOICE)
    comment = models.TextField(verbose_name='Комментарий к заказу', null=True, blank=True)
    created = models.DateTimeField(verbose_name='Создан', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Обновлен', auto_now=True)

    objects = models.Manager()

    def __str__(self):
        return self.order_id
