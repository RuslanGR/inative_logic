from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from apps.shop.models import Cart
from apps.shop.utils import generator
from .models import Order


@login_required
def checkout(request):
    """Checkout view, address and delivery details processing
    :param request:
    :return:
    """

    try:
        cart_id = request.session['cart_id']
    except KeyError:
        cart_id = generator(size=20)
        request.session['cart_id'] = cart_id

    cart, cart_created = Cart.objects.get_or_create(session=cart_id)
    if cart_created:
        print('cart created')

    order, order_created = Order.objects.get_or_create(cart=cart)

    if order_created:
        order.user = request.user
        order.order_id = generator(size=10)
        order.total_price = cart.total_price
        order.save()

    if order.status == Order.STATUS_FINISHED_CHOICE:
        try:
            del request.session['cart_id']
            del request.session['products_total']
        except KeyError:
            pass

    context = {}
    return render(request, 'orders/checkout.html', context)
