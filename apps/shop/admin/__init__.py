"""Admin"""

from django.contrib import admin

from ..models import (
    Product,
    Cart,
    CartItem
)
from .cart_item import CartItemAdmin
from .cart import CartAdmin


admin.site.register(Product)
admin.site.register(CartItem, CartItemAdmin)
admin.site.register(Cart, CartAdmin)
