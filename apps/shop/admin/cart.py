from django.contrib import admin

from ..models import Cart
from .inlines import CartItemInline


class CartAdmin(admin.ModelAdmin):

    inlines = (CartItemInline,)
    readonly_fields = ('session',)

    class Meta:
        model = Cart
