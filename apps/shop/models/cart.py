from django.db import models

from apps.central.models import InativeUser


class Cart(models.Model):
    """Cart model"""

    user = models.ForeignKey(verbose_name='Пользователь', to=InativeUser, on_delete=models.CASCADE,
                             null=True, blank=True)
    session = models.CharField(verbose_name='Ключ сессии', max_length=120, blank=True, null=True)
    active = models.BooleanField(verbose_name='Активный', default=True)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Обновлено', auto_now=True)

    @property
    def total_price(self):
        items = self.cartitem_set.all()
        total_price = 0
        for item in items:
            total_price += item.total_price
        return total_price

    class Meta:
        """Meta"""

        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзины'

    def __str__(self):
        return f'Корзина {self.pk}'
