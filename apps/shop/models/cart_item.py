from django.db import models

from .product import Product
from .cart import Cart


class CartItem(models.Model):
    """Cart item model"""

    cart = models.ForeignKey(verbose_name='Корзина', to=Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(verbose_name='Товар', to=Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(verbose_name='Количество', default=1)

    @property
    def total_price(self):
        """Find out total price"""
        return self.product.price * self.quantity

    class Meta:
        """Meta"""
        verbose_name = 'Предмет корзины'
        verbose_name_plural = 'Предметы корзины'

    def __str__(self):
        return f'{self.product.title} {self.quantity}'
