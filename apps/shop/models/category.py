from django.db import models


class Category(models.Model):

    subcategory = models.ForeignKey(verbose_name='Подкатегория', to='Category',
                                    on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(verbose_name='Название', max_length=120)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name
