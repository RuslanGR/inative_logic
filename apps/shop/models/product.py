from django.db import models
from django.utils.text import slugify
from django.urls import reverse

from .category import Category

from ..utils import transliterate


class Product(models.Model):
    """Product model"""

    img = models.ImageField(upload_to='products', null=True)
    title = models.CharField(verbose_name='Название', max_length=120)
    price = models.DecimalField(verbose_name='Цена', max_digits=10, decimal_places=2)
    slug = models.SlugField(verbose_name='Slug', blank=True, null=True)
    category = models.ForeignKey(verbose_name='Категория', to=Category, on_delete=models.CASCADE, null=True)

    class Meta:
        """Meta"""

        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def get_absolute_url(self):
        return reverse('product', args=[self.slug])

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        translated_text = transliterate(self.title)
        self.slug = slugify(translated_text)

        super().save(*args, **kwargs)
