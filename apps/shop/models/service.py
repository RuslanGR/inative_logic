from django.db import models


class Service(models.Model):
    """Services model"""

    title = models.CharField(verbose_name='Заголовок', max_length=240)
    description = models.TextField(verbose_name='Описание')
    price = models.DecimalField(verbose_name='Цена', max_digits=8, decimal_places=2)

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'

    def __str__(self):
        return self.title
