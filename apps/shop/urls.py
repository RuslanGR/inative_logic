from django.urls import path
from django.views.generic import TemplateView, ListView, DetailView

from .models import (
    Service,
    Cart,
)
from .views import (
    CategoryView,
    ProductView,

    update_cart,
    cart_view,
)
from apps.orders.views import checkout


urlpatterns = (
    path('', TemplateView.as_view(template_name='shop/index.html'), name='index'),
    path('services/', ListView.as_view(
        template_name='shop/services.html',
        model=Service,
        context_object_name='services'
    ), name='services'),
    path('shop/', TemplateView.as_view(template_name='shop/shop.html'), name='shop'),
    path('category/<slug:category>/', CategoryView.as_view(), name='category'),
    path('product/<slug:slug>', ProductView.as_view(), name='product'),
    path('cart/', cart_view, name='cart'),

    path('update-cart/<slug:slug>', update_cart, name='update-cart'),
    path('checkout/', checkout, name='checkout'),
)
