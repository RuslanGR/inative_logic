"""Views"""

from .cart import cart_view
from .product import ProductView
from .category import CategoryView
from .update_cart import update_cart
