from django.shortcuts import render

from ..utils import generator
from ..models import Cart


def cart_view(request):
    """Cart view"""
    if request.user.is_authenticated:
        cart, _ = Cart.objects.get_or_create(user=request.user)
        cart_id = cart.session
    else:
        try:
            cart_id = request.session['cart_id']
        except KeyError:
            cart_id = generator(size=20)
            request.session['cart_id'] = cart_id
        except Exception as err:
            print(str(err))

    cart, _ = Cart.objects.get_or_create(session=cart_id)

    return render(request, 'shop/cart.html', {'cart': cart})
