from django.shortcuts import render, get_list_or_404
from django.views import View

from ..models import Product


class CategoryView(View):
    """Products by category page view"""

    def get(self, request, category):
        """GET"""
        # TODO: Add category selections

        products = get_list_or_404(Product)

        context = {
            'products': products
        }
        return render(request, 'shop/category.html', context)
