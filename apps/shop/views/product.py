from django.views import View
from django.shortcuts import get_object_or_404, render

from ..models import Product


class ProductView(View):
    """Product view"""

    def get(self, request, slug):
        """GET"""
        # TODO: Change product view add category
        product = get_object_or_404(Product, slug=slug)
        return render(request, 'shop/product.html', {'product': product})
