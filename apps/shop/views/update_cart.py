from django.shortcuts import (
    get_object_or_404,
    HttpResponseRedirect,
    reverse,
)
from django.contrib import messages

from ..utils import generator
from ..models import (
    Product,
    Cart,
    CartItem
)


def update_cart(request, slug):
    """Add items to cart"""
    request.session.set_expiry(86400)  # Session expire after 1 day 86400

    if request.user.is_authenticated:
        cart, _ = Cart.objects.get_or_create(user=request.user)
    else:
        try:
            cart_id = request.session['cart_id']
        except KeyError:
            cart_id = generator(size=20)
            request.session['cart_id'] = cart_id
        except Exception as err:
            print(str(err))

        cart, _ = Cart.objects.get_or_create(session=cart_id)

    order = cart.order_set.all()
    if order.exists():
        if order[0].status == Cart.STATUS_STARTED_CHOICE:
            print('starter')

    product = get_object_or_404(Product, slug=slug)
    cart_item, _ = CartItem.objects.get_or_create(product=product, cart=cart)

    # Change quantity
    quantity = request.GET.get('quantity', None)
    if quantity:
        quantity = int(quantity)
        cart_item.quantity = quantity
        cart_item.save()
        messages.info(request, 'Item added')
    else:
        cart_item.delete()
        messages.info(request, 'Item removed')

    # Update products count information
    request.session['products_total'] = cart.cartitem_set.count()

    return HttpResponseRedirect(reverse('cart'))
